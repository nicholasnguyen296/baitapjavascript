/** 5. Bài tập tính tổng 2 ký số
 * 
 * Input
 * input = 44
 * 
 * To do:
 * ta cho num1= num1= Math.floor(input / 10) , num2= input % 10
 * là sẽ lấy được số tách.
 * 
 * 
 * 
 * Output:  tongSo = num1 + num2
 * 
 * 
 */
var input = 44
var num1= Math.floor(input / 10)
var num2= input % 10 
tongSo = num1 + num2

console.log('Số Ký:',input);
console.log('Num1:',num1);
console.log('Num2:',num2);
console.log('Tổng 2 Số Ký:',tongSo);
