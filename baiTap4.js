/** 4. Bài tập tính diện tích , chu vi
 * 
 * Input
 * chieuDai = 5
 * chieuRong = 10
 * 
 * To do:
 * muốn tính được diện tích :
 * ta lấy chieuDai * chieuRong
 * muốn tính được chu vi :
 * ta lấy ( chieuDai + chieuRong ) * 2
 * 
 * 
 * 
 * Output: 
 * dienTich = chieuDai * chieuRong
 * chuVi = ( chieuDai + chieuRong ) * 2
 * 
 */
var chieuDai = 5
var chieuRong = 10
var chuVi= 0
var dienTich = 0
dienTich = chieuDai * chieuRong
chuVi = (chieuDai + chieuRong) * 2
console.log('Chiều Dài :',chieuDai);
console.log('Chiều Rộng :',chieuRong);
console.log('Tổng diện tích :',dienTich);
console.log('Tổng chu vi :',chuVi);
